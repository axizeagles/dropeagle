<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ぼやき検索結果</title>
	<link rel="stylesheet" href="css/profile.css" type="text/css">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript">
	$(function($) {
		var tab = $('#side'), offset = tab.offset();
		$(window).scroll(function () {
			if($(window).scrollTop() > offset.top) {
				tab.addClass('fixed');
			} else {
				tab.removeClass('fixed');
			}
		});
	});
	</script>
<STYLE type="text/css">
<!--
html{background: url(wallpaper/nature/6.jpg) no-repeat center;}
-->
</STYLE>
</head>
<body>

<div id="tops">
	<p>
	 <div class="topsLeft ml_20 fl_l">検索結果画面です&nbsp;&nbsp;<span style="color:#fff;">所持ポイント：</span><span class="point ml_10">140pt</span></div><!--.topsLeft-->

         <div align="right" class="topsRight">
         <span class="mr_20"><a href="home.html">HOME</a></span>
         <span class="mr_20"><a href="profile.html">プロフィール編集</a></span>
         <span class="mr_20"><a href="login.html">ログアウト</a></span>
         <span class="mr_20 Userexit"><a href="userexit.html">退会</a></span>
         </div><!--.topsRight-->
	</p>
</div><!--#tops-->

<div id="wapper">

<div id="side">
		<center><img src="parts/logo.png"></center>
	    	<div class="login">
	    	<form>
			   <table cellpadding="0" cellspacing="0" border="0">
			        	<tr>
			             <td>
			             <center><p>いまなにしてる？</p></center>
			             <textarea rows="3" cols="30" class="boyakiBox"></textarea>
			             </td>
			            </tr>
			        </table>
                    <center><input type="file" value="変更"></center>
		         <br>
		        <center><img src="parts/boyakiButton.png"></center>
		        </form>
		        <br>
	        </div><!-- login -->

            <br>
			<center>
			<b style="color:red; font-weight:bold; font-size:16px;background-color:#fff;border:2px red solid;padding:2px;">ガチャ：1回300pt</b>
			<a href="gatya.html"><img src="parts/button01.png" width="258"></a></center>
            <br>
        	<center>
        	<b style="color:#FF9900; font-weight:bold; font-size:16px;background-color:#fff;border:2px #FF9900 solid;padding:2px;">レアガチャ：1回900pt</b>
        	<a href="gatya2.html"><img src="parts/button02.png" width="258"></a></center>

	</div><!-- side -->
	<br><br>

	<div id="container">

    	<p>
        <span class="Resultdate fl_l text_l">6月19日</span><span class="fl_l text_l ml_15">の検索結果</span>
        <span class="text_r ml_20">→<a href="home.html">戻る</a></span>
        </p>
		<br>


    	<table width="670" class="mt_10">
    		<tr>
    			<td width="60" valign="top"><a href="profileinfo.html"><img src="usericon/neko.png" width="60"></a></td>
    			<td>
    				<table width="600">
    					<tr><td align="left"><span style="color:blue; font-weight:bold;"><a href="profileinfo.html">永田登志雄</a></span><span class="ml_10 commentDate">6月19日14：42</span></td></tr>
    					<tr>
    					 <td>
    						<div class="commentBox">
    						パルムうめぇ～★☆今日はスーパーに言ったらマンゴー味が売ってた!!
    						</div>
    					 </td>
                        </tr>
    				</table>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="670">
    		<tr>
    			<td width="60" valign="top"><a href="profileinfo2.html"><img src="usericon/neko2.png" width="60"></a></td>
    			<td>
    				<table width="600">
    					<tr><td align="left"><span style="color:blue; font-weight:bold;"><a href="profileinfo2.html">林偉龍</a></span><span class="ml_10 commentDate">6月19日14：42</span></td></tr>
    					<tr>
    					 <td>
    						<div class="commentBox">
    						今日も元気だ、絶好調の林です！！
    						</div>
    					 </td>
                        </tr>
    				</table>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="670">
    		<tr>
    			<td width="60" valign="top"><a href="profile.html"><img src="usericon/neko3.png" width="60"></a></td>
    			<td>
    				<table width="600">
    					<tr><td align="left"><span style="color:blue; font-weight:bold;"><a href="profile.html">風見一慶</a></span><span class="ml_10 commentDate">6月19日14：42</span></td></tr>
    					<tr>
    					 <td>
    						<div class="commentBox">
    						今日は新しいいつも行く定食屋で、レモンラーメンを発見！！上にのった鶏肉もなかなかのボリュームで、暑い日にぴったり
    						</div>
    					 </td>
                        </tr>
    				</table>
    			</td>
    		</tr>
    	</table>
    <br>

    	<table width="670">
    		<tr>
    			<td width="60" valign="top"><a href="profile.html"><img src="usericon/neko.png" width="60"></a></td>
    			<td>
    				<table width="600">
    					<tr><td align="left"><span style="color:blue; font-weight:bold;"><a href="profile.html">森谷和俊</a></span><span class="ml_10 commentDate">6月19日14：42</span></td></tr>
    					<tr>
    					 <td>
    						<div class="commentBox">
    						ラブライブ☆
    						</div>
                            <div class="commentIMG">
                            <img src="dropgraphic/lovelive.jpg">
                            </div>
    					 </td>
                        </tr>
    				</table>
    			</td>
    		</tr>
    	</table>

    <br>

</div><!-- /#container -->

<div id="goal"></div><!-- goal -->

</div><!-- wapper -->

</body>
</html:html>