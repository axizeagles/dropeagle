<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザ検索結果</title>
	<link rel="stylesheet" href="../css/profile.css" type="text/css">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript">
	$(function($) {
		var tab = $('#Managerside'), offset = tab.offset();
		$(window).scroll(function () {
			if($(window).scrollTop() > offset.top) {
				tab.addClass('fixed');
			} else {
				tab.removeClass('fixed');
			}
		});
	});
	</script>
<STYLE type="text/css">
<!--
html{background: url(../wallpaper/nature/6.jpg) no-repeat center;}
-->
</STYLE>
</head>
<body>

<div id="tops">
	<p>
	 <div class="topsLeft ml_20 fl_l">ユーザ検索結果&nbsp;&nbsp;<span style="color:#fff;">所持ポイント：</span><span class="point ml_10">140pt</span></div><!--.topsLeft-->

         <div align="right" class="topsRight">
         <span class="mr_20"><a href="userinfo.html">管理画面</a></span>
         <span class="mr_20"><a href="home.html">HOME</a></span>
         <span class="mr_20"><a href="profile.html">プロフィール編集</a></span>
         <span class="mr_20"><a href="index.html">ログアウト</a></span>
         <span class="mr_20 Userexit"><a href="userexit.html">退会</a></span>
         </div><!--.topsRight-->
	</p>
</div><!--#tops-->

<div id="wapper">

	<div id="Managerside">
		<br>
		<br>
		<center><img src="../parts/logo.png"></center>
	    	<div class="login">
            	<br>
            	<center>検索結果表示</center>
		        <br>
	        </div><!-- login -->
	</div><!-- side -->
	<br><br>

	<div id="container">

        <p>
        <span class="fl_l text_l">ユーザアカウントの検索結果画面です</span>
        <span class="text_r ml_20"><a href="userinfo.html">管理画面ホームに戻る</a></span>
        </p>


        <div class="boyakiCount m_0 p_0">

        	<form>
            <div class="text_l fl_l">
        	<input type="text" size="40">
            <select>
            	<option value="Count10">名前で</option>
                <option value="Count20">IDで</option>
            </select>
            <a href="#"><input type="button" value="検索"></a>
            </div><!--text_l-->
            <div class="text_r">
        	表示件数：
        	<select>
            	<option value="Count10">10件</option>
                <option value="Count20">20件</option>
                <option value="Count30">30件</option>
                <option value="Count40">40件</option>
                <option value="Count50">50件</option>
                <option value="CountAll">全件</option>
        	</select>
            <input type="button" value="変更">
            </div><!--text_r-->
        	</form>

        </div><!--boyakiCount-->
        <br>
        <div class="acountDelete text_r">※削除するユーザにチェックをいれて削除ボタンを押してください<input type="button" value="削除"></div>

    	<table width="700">
    		<tr>
            	<td width="20" >
                <input type="checkbox" name="103" value="01">
                </td>
    			<td width="51">
    			 <img src="../usericon/neko.png" width="50">
    			</td>
    			<td width="613" align="left">
                	<span class="fl_l ml_10">ID:<span style="">103</span></span>
                	<span style="color:blue; font-weight:bold;" class="fl_l ml_10">永田登志雄</span>
                    <span class="text_r">
                    <form>
               		<a href="boyakiinfo.html"><input type="button" value="ぼやき表示"></a>
              		</form>

                    </span>
    			</td>
    		</tr>
    	</table>




    <br>

    <div class="acountDelete text_r">※削除するユーザにチェックをいれて削除ボタンを押してください<input type="button" value="削除"></div>

</div><!-- #container -->

<div id="goal"></div><!-- goal -->

</div><!-- wapper -->

</body>
</html:html>