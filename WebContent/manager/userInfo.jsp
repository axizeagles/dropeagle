<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザ管理</title>
	<link rel="stylesheet" href="../css/profile.css" type="text/css">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript">
	$(function($) {
		var tab = $('#Managerside'), offset = tab.offset();
		$(window).scroll(function () {
			if($(window).scrollTop() > offset.top) {
				tab.addClass('fixed');
			} else {
				tab.removeClass('fixed');
			}
		});
	});
	</script>
<STYLE type="text/css">
<!--
html{background: url(../wallpaper/nature/6.jpg) no-repeat center;}
-->
</STYLE>
</head>
<body>

<div id="tops">
	<p>
	 <div class="topsLeft ml_20 fl_l">ユーザ管理&nbsp;&nbsp;<span style="color:#fff;">所持ポイント：</span><span class="point ml_10">140pt</span></div><!--.topsLeft-->

         <div align="right" class="topsRight">
         <span class="mr_20"><a href="userinfo.html">管理画面</a></span>
         <span class="mr_20"><a href="home.html">HOME</a></span>
         <span class="mr_20"><a href="profile.html">プロフィール編集</a></span>
         <span class="mr_20"><a href="index.html">ログアウト</a></span>
         <span class="mr_20 Userexit"><a href="userexit.html">退会</a></span>
         </div><!--.topsRight-->
	</p>
</div><!--#tops-->


<div id="wapper">

	<div id="Managerside">
		<br>
		<br>
		<center><img src="../parts/logo.png"></center>
	    	<div class="login">
            	<br>
            	<center>ユーザアカウント一覧表示</center>
		        <br>
	        </div><!-- login -->
	</div><!-- side -->
	<br><br>

	<div id="container">

        <p>
        <span class="fl_l text_l">ユーザアカウントの編集画面です</span>
        <span class="text_r ml_20"><a href="index.html">管理画面終了</a></span>
        </p>


        <div class="boyakiCount m_0 p_0">

        	<form>
            <div class="text_l fl_l">

        	<input type="text" size="40" value="永田登志雄">
            <select>
            	<option value="Count10">名前で</option>
                <option value="Count20">IDで</option>
            </select>
            <a href="searchresultAdmin.html"><input type="button" value="検索"></a>
            </div><!--text_l-->
            <div class="text_r">
        	表示件数：
        	<select>
            	<option value="Count10">10件</option>
                <option value="Count20">20件</option>
                <option value="Count30">30件</option>
                <option value="Count40">40件</option>
                <option value="Count50">50件</option>
                <option value="CountAll">全件</option>
        	</select>
            <input type="button" value="変更">
            </div><!--text_r-->
        	</form>

        </div><!--boyakiCount-->
        <br>
        <div class="acountDelete text_r">※削除するユーザにチェックをいれて削除ボタンを押してください<a href="deletecheck.html"><input type="button" value="削除"></a></div>

    	<table width="700">
    		<tr>
            	<td width="20" >
                <input type="checkbox" name="103" value="01">
                </td>
    			<td width="51">
    			 <img src="../usericon/neko.png" width="50" height="50">
    			</td>
    			<td width="613" align="left">
                	<span class="fl_l ml_10">ID:<span style="">103</span></span>
                	<span style="color:blue; font-weight:bold;" class="fl_l ml_10">永田登志雄</span>
                    <span class="text_r">
                    <form>
               		<a href="boyakiinfo.html"><input type="button" value="ぼやき表示"></a>
              		</form>

                    </span>
    			</td>
    		</tr>
    	</table>


    <br>

    	<table width="700">
    		<tr>
            	<td width="20" >
                <input type="checkbox" name="103" value="01">
                </td>
    			<td width="51">
    			 <img src="../usericon/neko2.png" width="50" height="50">
    			</td>
    			<td width="613" align="left">
                	<span class="fl_l ml_10">ID:<span style="">115</span></span>
                	<span style="color:blue; font-weight:bold;" class="fl_l ml_10">林偉龍</span>
                    <span class="text_r">
                    <form>
               		<a href="boyakiinfo.html"><input type="button" value="ぼやき表示"></a>
              		</form>

                    </span>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="700">
    		<tr>
            	<td width="20" >
                <input type="checkbox" name="103" value="01">
                </td>
    			<td width="51">
    			 <img src="../usericon/neko3.png" width="50" height="50">
    			</td>
    			<td width="613" align="left">
                	<span class="fl_l ml_10">ID:<span style="">106</span></span>
                	<span style="color:blue; font-weight:bold;" class="fl_l ml_10">風見一慶</span>
                    <span class="text_r">
                    <form>
               		<a href="boyakiinfo.html"><input type="button" value="ぼやき表示"></a>
              		</form>

                    </span>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="700">
    		<tr>
            	<td width="20" >
                <input type="checkbox" name="103" value="01">
                </td>
    			<td width="51">
    			 <img src="../usericon/radiokaikan.jpg" width="50" height="50">
    			</td>
    			<td width="613" align="left">
                	<span class="fl_l ml_10">ID:<span style="">144</span></span>
                	<span style="color:blue; font-weight:bold;" class="fl_l ml_10">森谷和俊</span>
                    <span class="text_r">
                    <form>
               		<a href="boyakiinfo.html"><input type="button" value="ぼやき表示"></a>
              		</form>

                    </span>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="700">
    		<tr>
            	<td width="20" >
                <input type="checkbox" name="103" value="01">
                </td>
    			<td width="51">
    			 <img src="../usericon/neko4.jpg" width="50" height="50">
    			</td>
    			<td width="613" align="left">
                	<span class="fl_l ml_10">ID:<span style="">177</span></span>
                	<span style="color:blue; font-weight:bold;" class="fl_l ml_10">香月兼一</span>
                    <span class="text_r">
                    <form>
               		<a href="boyakiinfo.html"><input type="button" value="ぼやき表示"></a>
              		</form>

                    </span>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="700">
    		<tr>
            	<td width="20" >
                <input type="checkbox" name="103" value="01">
                </td>
    			<td width="51">
    			 <img src="../usericon/im@s01.jpg" width="50" height="50">
    			</td>
    			<td width="613" align="left">
                	<span class="fl_l ml_10">ID:<span style="">145</span></span>
                	<span style="color:blue; font-weight:bold;" class="fl_l ml_10">西山広敏</span>
                    <span class="text_r">
                    <form>
               		<a href="boyakiinfo.html"><input type="button" value="ぼやき表示"></a>
              		</form>

                    </span>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="700">
    		<tr>
            	<td width="20" >
                <input type="checkbox" name="103" value="01">
                </td>
    			<td width="51">
    			 <img src="../usericon/jojo.jpg" width="50" height="50">
    			</td>
    			<td width="613" align="left">
                	<span class="fl_l ml_10">ID:<span style="">154</span></span>
                	<span style="color:blue; font-weight:bold;" class="fl_l ml_10">田頭克矩</span>
                    <span class="text_r">
                    <form>
               		<a href="boyakiinfo.html"><input type="button" value="ぼやき表示"></a>
              		</form>

                    </span>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="700">
    		<tr>
            	<td width="20" >
                <input type="checkbox" name="103" value="01">
                </td>
    			<td width="51">
    			 <img src="../usericon/odenkun.jpg" width="50" height="50">
    			</td>
    			<td width="613" align="left">
                	<span class="fl_l ml_10">ID:<span style="">445</span></span>
                	<span style="color:blue; font-weight:bold;" class="fl_l ml_10">深谷俊</span>
                    <span class="text_r">
                    <form>
               		<a href="boyakiinfo.html"><input type="button" value="ぼやき表示"></a>
              		</form>

                    </span>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="700">
    		<tr>
            	<td width="20" >
                <input type="checkbox" name="103" value="01">
                </td>
    			<td width="51">
    			 <img src="../usericon/marshall.jpg" width="50" height="50">
    			</td>
    			<td width="613" align="left">
                	<span class="fl_l ml_10">ID:<span style="">477</span></span>
                	<span style="color:blue; font-weight:bold;" class="fl_l ml_10">松本諒</span>
                    <span class="text_r">
                    <form>
               		<a href="boyakiinfo.html"><input type="button" value="ぼやき表示"></a>
              		</form>

                    </span>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="700">
    		<tr>
            	<td width="20" >
                <input type="checkbox" name="103" value="01">
                </td>
    			<td width="51">
    			 <img src="../usericon/gyuutan.jpg" width="50" height="50">
    			</td>
    			<td width="613" align="left">
                	<span class="fl_l ml_10">ID:<span style="">221</span></span>
                	<span style="color:blue; font-weight:bold;" class="fl_l ml_10">横田海</span>
                    <span class="text_r">
                    <form>
               		<a href="boyakiinfo.html"><input type="button" value="ぼやき表示"></a>
              		</form>

                    </span>
    			</td>
    		</tr>
    	</table>


    <br>

    <div class="acountDelete text_r">※削除するユーザにチェックをいれて削除ボタンを押してください<input type="button" value="削除"></div>

</div><!-- #container -->

<div id="goal"></div><!-- goal -->

</div><!-- wapper -->

</body>
</html:html>