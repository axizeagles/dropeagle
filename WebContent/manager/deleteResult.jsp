<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>データ削除結果</title>
	<link rel="stylesheet" href="../css/profile.css" type="text/css">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript">
	$(function($) {
		var tab = $('#Managerside'), offset = tab.offset();
		$(window).scroll(function () {
			if($(window).scrollTop() > offset.top) {
				tab.addClass('fixed');
			} else {
				tab.removeClass('fixed');
			}
		});
	});
	</script>
<STYLE type="text/css">
<!--
html{background: url(../wallpaper/nature/6.jpg) no-repeat center;}
-->
</STYLE>
</head>
<body>

<div id="tops">
	<p>
	 <div class="topsLeft ml_20 fl_l">データ削除確認&nbsp;&nbsp;<span style="color:#fff;">所持ポイント：</span><span class="point ml_10">140pt</span></div><!--.topsLeft-->

         <div align="right" class="topsRight">
         <span class="mr_20"><a href="../userinfo.html">管理画面</a></span>
         <span class="mr_20"><a href="../home.html">HOME</a></span>
         <span class="mr_20"><a href="../profile.html">プロフィール編集</a></span>
         <span class="mr_20"><a href="../index.html">ログアウト</a></span>
         <span class="mr_20 Userexit"><a href="../userexit.html">退会</a></span>
         </div><!--.topsRight-->
	</p>
</div><!--#tops-->

<div id="wapper">

	<div id="Managerside">
		<br>
		<br>
		<center><img src="../parts/logo.png"></center>
	    	<div class="login">
            	<br>
            	<center>データ削除確認</center>
		        <br>
	        </div><!-- login -->
	</div><!-- side -->
	<br><br>

	<div id="container">

        <p>
        <span class="fl_l text_l">ユーザアカウントのデータ削除確認画面です</span>
        <span class="text_r ml_20"><a href="userinfo.html">管理画面ホームに戻る</a></span>
        </p>


        <div class="boyakiCount m_0 p_0">


        </div><!--boyakiCount-->
        <br>

          <div class="boyakiCount m_0 p_0">

        	<form>

            <div class="text_r">
 <br> <br> <br> <br>

               		   <center>
    			<b style="color:red;">削除しました</b>
              		</center>
			</div>
        	</form>



    <br>


</div><!-- #container -->

<div id="goal"></div><!-- goal -->

</div><!-- wapper -->

</body>
</html:html>