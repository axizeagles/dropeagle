<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>コレクション編集</title>
	<link rel="stylesheet" href="css/profile.css" type="text/css">
<STYLE type="text/css">
<!--
html{background: url(wallpaper/nature/6.jpg) no-repeat center;}
-->
</STYLE>
</head>


<body>

<div id="tops">
	<p>
	 <div class="topsLeft ml_20 fl_l">永田登志雄さん 編集画面<span class="point ml_10">140pt</span></div><!--.topsLeft-->

         <div align="right" class="topsRight">
         <span class="mr_20"><a href="home.html">HOME</a></span>
         <span class="mr_20"><a href="profile.html">プロフィール編集</a></span>
         <span class="mr_20"><a href="index.html">ログアウト</a></span>
         <span class="mr_20 Userexit"><a href="userexit.html">退会</a></span>
         </div><!--.topsRight-->
	</p>
</div><!--#tops-->

<div id="wapper">

<br>

<div id="profile">
<br>

<center>
<table width="820">
	<tr>
     <td>

     <div class="boyakiCount m_0 p_0">
        	<br>
            <div class="text_l fl_l ml_15">
            <a href="profile.html">プロフィール編集画面へ戻る</a>
            </div><!--text_l-->
        	<form>
            <div class="text_r" style="width:800px;">
            背景種類：
        	<select>
            	<option value="Count10">自然</option>
            	<option value="Count10">動物</option>
                <option value="Count20">世界遺産</option>
        	</select>
            <a href="bginfo2.html"><input type="button" value="背景種類変更"></a>
        	表示件数：
        	<select>
            	<option value="Count10">14件</option>
            	<option value="Count10">28件</option>
                <option value="Count20">42件</option>
                <option value="Count30">56件</option>
                <option value="Count40">70件</option>
                <option value="Count50">84件</option>
                <option value="CountAll">全件</option>
        	</select>
            <input type="button" value="変更">
            </div><!--text_r-->
        	</form>

        </div><!--boyakiCount-->

     </td>
    </tr>
    <tr>
     <td>
     <br>
     <center>
     <span style="color:red;">※変更したい背景画像にチェックをしてボタンを押してください</span><br><br>
     <input type="button" value="背景画像変更">
     </center>
     </td>
    </tr>
</table>
</center>

<center>
<table width="820"><!-- コレクションtable -->
	<tr>
     <td>
		<div class="ml_10 mt_10 fl_l">
        <label for="a1"><img src="wallpaper/nature/1.jpg" width="190" height="120"></label><br>
        <center><input type="radio" name="SENTAKU" id="a1" value="1"></center>
        </div>
        <div class="ml_10 mt_10 fl_l">
        <label for="a2"><img src="wallpaper/nature/2.jpg" width="190" height="120"></label><br>
        <center><input type="radio" name="SENTAKU" id="a2" value="2"></center>
        </div>
        <div class="ml_10 mt_10 fl_l">
        <label for="a3"><img src="wallpaper/nature/3.jpg" width="190" height="120"></label><br>
        <center><input type="radio" name="SENTAKU" id="a3" value="3"></center>
        </div>
        <div class="ml_10 mt_10 fl_l">
        <label for="a4"><img src="wallpaper/nature/4.jpg" width="190" height="120"></label><br>
        <center><input type="radio" name="SENTAKU" id="a4" value="4"></center>
        </div>
        <div class="ml_10 mt_10 fl_l">
        <label for="a5"><img src="wallpaper/nature/5.jpg" width="190" height="120"></label><br>
        <center><input type="radio" name="SENTAKU" id="a5" value="5"></center>
        </div>
        <div class="ml_10 mt_10 fl_l">
        <label for="a6"><img src="wallpaper/nature/6.jpg" width="190" height="120"></label><br>
        <center><input type="radio" name="SENTAKU" id="a6" value="6"></center>
        </div>
        <div class="ml_10 mt_10 fl_l">
        <label for="a7"><img src="wallpaper/nature/7.jpg" width="190" height="120"></label><br>
        <center><input type="radio" name="SENTAKU" id="a7" value="7"></center>
        </div>
        <div class="ml_10 mt_10 fl_l">
        <label for="a8"><img src="wallpaper/nature/8.jpg" width="190" height="120"></label><br>
        <center><input type="radio" name="SENTAKU" id="a8" value="8"></center>
        </div>
        <div class="ml_10 mt_10 fl_l">
        <label for="a9"><img src="wallpaper/nature/9.jpg" width="190" height="120"></label><br>
        <center><input type="radio" name="SENTAKU" id="a9" value="9"></center>
        </div>
        <div class="ml_10 mt_10 fl_l">
        <label for="a10"><img src="wallpaper/nophoto/nophoto.jpg" width="190" height="120"></label><br>
        <center><input type="radio" name="SENTAKU" id="#" value="#"></center>
        </div>
        <div class="ml_10 mt_10 fl_l">
        <label for="a10"><img src="wallpaper/nophoto/nophoto.jpg" width="190" height="120"></label><br>
        <center><input type="radio" name="SENTAKU" id="#" value="#"></center>
        </div>
        <div class="ml_10 mt_10 fl_l">
        <label for="a10"><img src="wallpaper/nophoto/nophoto.jpg" width="190" height="120"></label><br>
        <center><input type="radio" name="SENTAKU" id="#" value="#"></center>
        </div>
        <div class="ml_10 mt_10 fl_l">
        <label for="a10"><img src="wallpaper/nophoto/nophoto.jpg" width="190" height="120"></label><br>
        <center><input type="radio" name="SENTAKU" id="#" value="#"></center>
        </div>
        <div class="ml_10 mt_10 fl_l">
        <label for="a10"><img src="wallpaper/nophoto/nophoto.jpg" width="190" height="120"></label><br>
        <center><input type="radio" name="SENTAKU" id="#" value="#"></center>
        </div>
        <div class="ml_10 mt_10 fl_l">
        <label for="a10"><img src="wallpaper/nophoto/nophoto.jpg" width="190" height="120"></label><br>
        <center><input type="radio" name="SENTAKU" id="#" value="#"></center>
        </div>
        <div class="ml_10 mt_10 fl_l">
        <label for="a10"><img src="wallpaper/nophoto/nophoto.jpg" width="190" height="120"></label><br>
        <center><input type="radio" name="SENTAKU" id="#" value="#"></center>
        </div>
        <div class="cl_b mb_10"></div>
     </td>
    </tr>
</table><!-- コレクションtable -->
</center>

<center>
<table width="820">
	<tr>
     <td>
     <br>
     <center>
     <span style="color:red;">※変更したい背景画像にチェックをしてボタンを押してください</span><br><br>
     <input type="button" value="背景画像変更">
     </center>
     <br>
     </td>
    </tr>
</table>
</center>

<br>


</div><!-- profile -->

<div id="goal"></div><!-- goal -->

</div><!-- bgInfo -->

</body>
</html:html>