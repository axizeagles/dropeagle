<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>プロフィール編集</title>
	<link rel="stylesheet" href="css/profile.css" type="text/css">
<STYLE type="text/css">
<!--
html{background: url(wallpaper/nature/6.jpg) no-repeat center;}
-->
</STYLE>
</head>

<body>

<div id="tops">
	<p>
	 <div class="topsLeft ml_20 fl_l">永田登志雄さん 編集画面<span class="point ml_10">140pt</span></div><!--.topsLeft-->

         <div align="right" class="topsRight">
	     	<span class="mr_20"><a href="home.html">HOME</a></span>
         	<span class="mr_20"><a href="profile.html">プロフィール編集</a></span>
         	<span class="mr_20"><a href="index.html">ログアウト</a></span>
         	<span class="mr_20 Userexit"><a href="userexit.html">退会</a></span>
         </div><!--.topsRight-->
	</p>
</div><!--#tops-->

<div id="wapper">

<br>

<div id="profile">
<br>
<center>
<table width="820"><!-- 大枠 -->
	<tr>
     <td width="236">

		<div class="profileIMG">
            <img src="usericon/neko.png" width="140" height="140">
            <br>
            <center><form><input type="file" value="変更"></form></center>
        </div><!--profileIMG-->

     </td>
     <td width="317">

		<table><!-- プロフィールtable -->
           <tr>
            <td align="left">お名前：</td>
            <td align="left"><input type="text" value="永田登志雄" size="20" ><br></td>
           </tr>
           <tr>
          	<td align="left">出身：</td>
            <td align="left"><input type="text" value="すすきの" size="20"><br></td>
           </tr>
           <tr>
            <td align="left">PASS：</td>
            <td align="left"><input type="text" value="pokemon" size="20"><br></td>
           </tr>
           <tr>
            <td align="left">PASS：<br>(再確認)</td>
            <td align="left"><input type="text" value="pokemon" size="20"><br></td>
           </tr>
           <tr>
            <td align="left">コメント：</td>
            <td align="left"><textarea  cols="40" rows="6" class="boyakiBox">こんにちは、私はとても人見知りで普段はおとなしい性格です。</textarea></td>
           </tr>
    　　</table><!-- プロフィールtable -->

     </td>
     <td width="231" rowspan="2">
		<div class="gatyainfo">
        <br>
        <div class="gatyainfoTEXT">
         好きな背景画像の種類を<br>
		 選んでガチャを回そう!!
        </div><!--gatyainfoTEXT-->
        <br>
        <form>
        <select class="mb_10">
        	<option value="Count10">自然</option>
            <option value="Count20">動物</option>
            <option value="Count20">世界遺産</option>
        </select>
        </form>

        </div><!--gatyainfo-->
        <br>
        <center><b style="color:red; font-weight:bold; font-size:16px;background-color:#fff;border:2px red solid;padding:2px;">ガチャ：1回300pt</b></center>
		<center><a href="gatya.html"><img src="parts/button01.png" width="200"></a></center>
        <br>
        <center><b style="color:#FF9900; font-weight:bold; font-size:16px;background-color:#fff;border:2px #FF9900 solid;padding:2px;">レアガチャ：1回900pt</b></center>
        <center><a href="gatya2.html"><img src="parts/button02.png" width="200"></a></center>
     </td>
	</tr>

    <tr><td colspan="2" align="center"><div class="SUBMIT"><input type="submit" value="プロフィール変更" class="mt_10"></div></td><td></td></tr>

</table>
</center>

<center>
<table width="820"><!--背景画像table-->
	<tr>
     <td>

     <span class="ml_10">背景画像選択：</span>
     <table class="mt_10 ml_10"><!-- 背景画像選択テーブル -->
      <tr>
        <td><label for="a2"><img src="wallpaper/nature/1.jpg" width="80" height="60"></label>
          <br>
          <center>
            <input type="radio" name="SENTAKU" id="a2" value="1">
          </center></td>
        <td><label for="a3"><img src="wallpaper/nature/2.jpg" width="80" height="60"></label>
          <br>
          <center>
            <input type="radio" name="SENTAKU" id="a3" value="2">
          </center></td>
        <td><label for="a4"><img src="wallpaper/nature/3.jpg" width="80" height="60"></label>
          <br>
          <center>
            <input type="radio" name="SENTAKU" id="a4" value="3">
          </center></td>
        <td><label for="a5"><img src="wallpaper/nature/4.jpg" width="80" height="60"></label>
          <br>
          <center>
            <input type="radio" name="SENTAKU" id="a5" value="4">
          </center></td>
        <td><label for="a6"><img src="wallpaper/nature/5.jpg" width="80" height="60"></label>
          <br>
          <center>
            <input type="radio" name="SENTAKU" id="a6" value="5">
          </center></td>
        <td><label for="a7"><img src="wallpaper/nature/6.jpg" width="80" height="60"></label>
          <br>
          <center>
            <input type="radio" name="SENTAKU" id="a7" value="6">
          </center></td>
        <td><label for="a8"><img src="wallpaper/nature/7.jpg" width="80" height="60"></label>
          <br>
          <center>
            <input type="radio" name="SENTAKU" id="a8" value="7">
          </center></td>
        <td><label for="a9"><img src="wallpaper/nature/8.jpg" width="80" height="60"></label>
          <br>
          <center>
            <input type="radio" name="SENTAKU" id="a9" value="8">
          </center></td>
        <td><label for="a10"><img src="wallpaper/animal/ani02.jpg" width="80" height="60"></label>
          <br>
          <center>
            <input type="radio" name="SENTAKU" id="a10" value="9">
          </center></td>
      </tr>
    </table><!-- 背景画像選択テーブル -->
    <br>
    <div class="album"><input type="button" value="背景画像変更" class="mr_150"><a href="bginfo.html">背景画像コレクション一覧を見る</a></div>
     <br>
     </td>
    </tr>
</table><!--背景画像table-->
</center>

<br>
		<center>
		<div style="width:820px;">

       <div class="boyakiCount m_0 p_0">

        	<form>
            <div class="text_l fl_l">
        	<input type="text" size="40">
            <select>
            	<option value="Count10">日付で</option>
                <option value="Count20">キーワードで</option>
            </select>
            <a href="profileresult.html"><input type="button" value="検索"></a>
            </div><!--text_l-->
            <div class="text_r">
        	表示件数：
        	<select>
            	<option value="Count10">10件</option>
                <option value="Count20">20件</option>
                <option value="Count30">30件</option>
                <option value="Count40">40件</option>
                <option value="Count50">50件</option>
                <option value="CountAll">全件</option>
        	</select>
            <input type="button" value="変更">
            </div><!--text_r-->
        	</form>

        </div><!--boyakiCount-->
        <br>
        <div class="acountDelete text_r">※削除するぼやきにチェックをいれて削除ボタンを押してください<input type="button" value="削除"></div>

        <br>

       <table width="820">
    		<tr>
            	<td width="20" >
                <input type="checkbox" name="103" value="01">
                </td>
    			<td width="613" align="left">
                	<span class="ml_10 commentDate">6月19日14：42</span>
                	<div class="commentBox">
                    パルムうめぇ～★☆今日はスーパーに言ったらマンゴー味が売ってた!!
                    </div>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="820">
    		<tr>
            	<td width="20" >
                <input type="checkbox" name="103" value="01">
                </td>
    			<td width="613" align="left">
                	<span class="ml_10 commentDate">6月19日13：22</span>
                	<div class="commentBox">おなかすいた～お昼なに食べよかな</div>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="820">
    		<tr>
            	<td width="20" >
                <input type="checkbox" name="103" value="01">
                </td>
    			<td width="613" align="left">
                	<span class="ml_10 commentDate">6月18日17：42</span>
                	<div class="commentBox">うわっ雨ふってる！だれか傘くれ～</div>
    			</td>
    		</tr>
    	</table>


    <br>

    	<table width="820">
    		<tr>
            	<td width="20" >
                <input type="checkbox" name="103" value="01">
                </td>
    			<td width="613" align="left">
                	<span class="ml_10 commentDate">6月18日10：01</span>
                	<div class="commentBox">目の前で痴漢がつかまった！おれはやってませんよ</div>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="820">
    		<tr>
            	<td width="20" >
                <input type="checkbox" name="103" value="01">
                </td>
    			<td width="613" align="left">
                	<span class="ml_10 commentDate">6月18日09：10</span>
                	<div class="commentBox">電車遅延。。早起きした意味が。。</div>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="820">
    		<tr>
            	<td width="20" >
                <input type="checkbox" name="103" value="01">
                </td>
    			<td width="613" align="left">
                	<span class="ml_10 commentDate">6月17日12：13</span>
                	<div class="commentBox">犬にやたら吠えられてる！！敵だと思われてるのかな。。</div>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="820">
    		<tr>
            	<td width="20" >
                <input type="checkbox" name="103" value="01">
                </td>
    			<td width="613" align="left">
                	<span class="ml_10 commentDate">6月16日10.：50</span>
                	<div class="commentBox">
                    うぉ～ツカイツリー近くで見るとでけぇ～
                    </div>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="820">
    		<tr>
            	<td width="20" >
                <input type="checkbox" name="103" value="01">
                </td>
    			<td width="613" align="left">
                	<span class="ml_10 commentDate">6月15日14：40</span>
                	<div class="commentBox">海鮮ドン！！ドン！！ドン！！</div>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="820">
    		<tr>
           	  <td width="20" >
                <input type="checkbox" name="103" value="01">
              </td>
    			<td width="613" align="left">
                	<span class="ml_10 commentDate">6月15日20：00</span>
                	<div class="commentBox">寝起き最悪。。なぜか肉食いたい！</div>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="820">
    		<tr>
           	  <td width="20" >
                <input type="checkbox" name="103" value="01">
              </td>
    			<td width="613" align="left">
                	<span class="ml_10 commentDate">6月14日12：00</span>
                	<div class="commentBox">うぉ～～テンション下がる↓↓↓↓</div>
    			</td>
    		</tr>
    	</table>

    <br>
        <div class="acountDelete text_r">※削除するぼやきにチェックをいれて削除ボタンを押してください<input type="button" value="削除"></div>
        <br>


       </div>
       </center>

</div><!-- profile -->

<div id="goal"></div><!-- goal -->

</div><!-- wapper -->

</body>
</html:html>