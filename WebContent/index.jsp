<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ログイン</title>
	<link rel="stylesheet" href="css/css.css" type="text/css">
</head>
<body>

<div id="tops">
    <p>
	 <div class="topsLeft ml_20 fl_l">Dropへようこそ</div>
         <div align="right" class="topsRight">
         <span class="mr_20">参加人数<span style="color:#FFF; font-weight:bold; font-size:20px; padding:2px;">10</span>人</span>
         </div>
	</p>
</div>

<center>
<div id="loginPage">

		<div class="login">
        <br>
        <img src="parts/logo.png">
        <br>
    	<table cellpadding="0" cellspacing="0" border="0" width="140">
	    	<tr>
	    	<td><center>ちょっと内向的なミニブログ<br>
	    		Dropへようこそ</center><br>
	    		<html:errors/>
	    		<html:form action="/IndexForm">
	    	</td>
	    	</tr>
    		<tr>
             <td>
             ID：<html:password property="userid"  name="IndexForm" value="${param.userid}"/><br>
             PASS：<html:password property="pass"  name="IndexForm" value="${param.pass}"/><br><br>
             </td>
            </tr>
        </table>
        </div><!-- login -->
        <center><html:submit value="ログイン"/></html:form></center><br>
        <center><a href="userInsert.jsp"><img src="parts/insertbutoon.jpg"></a></center><br>

</div><!-- #loginPage -->
</center>

<div id="goal"></div><!-- goal -->

</body>
</html:html>