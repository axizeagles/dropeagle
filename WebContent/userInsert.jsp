<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザ登録</title>
	<link rel="stylesheet" href="css/css.css" type="text/css">
</head>
<body>

<div id="tops">
	<p>
	 <div class="topsLeft ml_20 fl_l">Dropへようこそ</div><!-- .topsLeft .ml_20 .fl_l -->

         <div align="right" class="topsRight">
         	<span class="mr_20"><a href="index.jsp">戻る</a></span>
         </div><!-- .topsRight -->
	</p>
</div><!-- #tops -->

<center>
<div id="loginPage">

		<div class="login">
        <br>
        <img src="parts/logo.png">
        <br>
        <center>ID・お名前・PASSを入力してください</center>
		<html:errors/>
		<html:form action="/UserInsertForm">
        <br>
        <center>
    	<table cellpadding="0" cellspacing="0" border="0">
    		<tr>
             <td>
             <span style="color:red; font-size:12px;">※</span>ID：<html:text property="userid"  name="UserInsertForm" value="${param.userid}"/><br>
             <span style="color:red; font-size:12px;">※</span>お名前：<html:text property="username"  name="UserInsertForm" value="${param.username}"/><br>
             <span style="color:red; font-size:12px;">※</span>PASS：<html:password property="pass1"  name="UserInsertForm" value="${param.pass1}"/><br>
             <span style="color:red; font-size:12px;">※</span>PASS：<html:password property="pass2"  name="UserInsertForm" value="${param.pass2}"/><br>
            <center><div style="text-align:left; width:200px;" class="mr_l30">(再確認)</div></center>
             </td>
            </tr>
        </table>
        </center>
        </div><!-- login -->
        <center><html:submit value="登録"/></center><br>
		</html:form>
</div><!-- #loginPage -->
</center>

<div id="goal"></div><!-- goal -->

</body>
</html:html>