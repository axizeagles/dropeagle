<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>退会確認</title>
	<link rel="stylesheet" href="css/css.css" type="text/css">
</head>
<body>

<div id="tops">
    <p>
	 <div class="topsLeft ml_20 fl_l">退会確認</div><!--.topsLeft-->

         <div align="right" class="topsRight">
         <span class="mr_20"><a href="home.html">HOME</a></span>
         <span class="mr_20"><a href="profile.html">プロフィール編集</a></span>
         <span class="mr_20"><a href="index.html">ログアウト</a></span>
         <span class="mr_20 Userexit"><a href="userexit.html">退会</a></span>
         </div><!--.topsRight-->
	</p>
</div>

<center>
<div id="userexitPage">

		<div class="login">
        <br>
        <img src="parts/logo.png">

        <center>
        <table>
        	<tr>
             <td>
              <div class="Questionnaire">
              	  <center>
              	   <span>
              	  Dropのご利用ありがとうございました。<br>
              	  Dropの退会について以下のアンケートにご協力をお願いします。<br><br>
              	  </span>
                  <div class="choice">
                  <form>
                  <span>Q1:退会理由をお選びください(複数選択可)</span><br>
                  <input type="checkbox" name="riyu" value="1">ガチャに飽きたから<br><br>
                  <input type="checkbox" name="riyu" value="1" checked>ぼやきたくなくなったから<br><br>
                  <input type="checkbox" name="riyu" value="1" checked>利用する回数が減ったから<br><br>
                  <input type="checkbox" name="riyu" value="1">他のサイトを利用しているから<br><br>
                  <input type="checkbox" name="riyu" value="1">機能が少ないから<br><br>
                  <input type="checkbox" name="riyu" value="1" checked>忙しいから<br><br>
                  <input type="checkbox" name="riyu" value="1" checked>面白いぼやきがないから<br><br>
                  <span>Q2:今後の参考のためにご意見をお願い致します。（必須）</span>
                  <textarea rows="4" cols="60" class="box">最近他のSNSを利用しているため、このサイトを利用しなくなりました。また機会があれば利用しようと思いますが今回は退会します。</textarea><br><br>
                  <span>※ID、PASSを入力してください。</span><br>
                  ID：<input type="text" class="ml_30" value="105"/><br>
             	  PASS：<input type="text" value="tadaima"/><br><br>
                  </form>
                  </div><!--choice-->
                  </center>
              </div><!--Questionnaire-->
             </td>
            </tr>
        </table>
        </center>


        </div><!-- login -->
        <center>上記の内容でよろしいでしょうか？</center>
        <center><a href="userexitend.html"><input type="button" value="退会する"/></a><input type="reset" value="リセット"/></center><br>

</div><!-- #loginPage -->
</center>

<div id="goal"></div><!-- goal -->

</body>
</html:html>