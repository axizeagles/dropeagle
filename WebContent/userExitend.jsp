<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>退会結果</title>
	<link rel="stylesheet" href="css/css.css" type="text/css">
</head>
<body>

<div id="tops">
    <p>
	 <div class="topsLeft ml_20 fl_l">退会結果画面</div><!--.topsLeft-->

         <div align="right" class="topsRight">
         <span class="mr_20"><a href="index.html">ログイン画面</a>
         </div><!--.topsRight-->
	</p>
</div>

<center>
<div id="loginPage">

		<div class="login">
        <br>
        <img src="parts/logo.png">
        <br>
        <br>
        <br>
        <center>
        	<span style="color:red;">またのご利用お待ちしております</span>
        </center>
        <br>
        <br>
        <br>
        </div><!-- login -->

</div><!-- #userexitPage -->
</center>

<div id="goal"></div><!-- goal -->

</body>
</html:html>