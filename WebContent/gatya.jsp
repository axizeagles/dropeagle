<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ガチャ</title>
	<link rel="stylesheet" href="css/gatya.css" type="text/css">
</head>
<body>

<div id="tops">
     <p>
	 <div class="topsLeft ml_20 fl_l">ガチャを回す</div><!-- topsLeft -->

         <div align="right" class="topsRight">
         <span class="mr_20"><a href="home.html">戻る</a></span>
         </div><!-- topsRight -->

	</p>
</div><!-- tops -->

<center>
<div style="margin-top:100px; color:#000;" class="gatya">
		<center><img src="parts/gatya03.gif" width="200"></center><br>
		<center>300ポイントで、1回ガチャを回せます。</center>
		<center><span class="name">永田登志雄</span>さんのポイントは<span class="point">300</span>ポイントです。</center>
		<br>
		<center><a href="gatyaresult.html"><img src="parts/gatyabutton.jpg" width="258" height="68"></a></center>

</div><!-- gatya -->
</center>

<div id="goal"></div><!-- goal -->

</body>
</html:html>