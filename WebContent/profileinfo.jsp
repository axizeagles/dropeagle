<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザプロフィール</title>
	<link rel="stylesheet" href="css/profile.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="resource/lightbox.css" media="screen,tv" />
    <script type="text/javascript" charset="UTF-8" src="resource/lightbox_plus_min.js"></script>
<STYLE type="text/css">
<!--
html{background: url(wallpaper/nature/6.jpg) no-repeat center;}
-->
</STYLE>
</head>

<body>
<div id="tops">
	<p>
	 <div class="topsLeft ml_20 fl_l">永田登志雄さんのプロフィール画面です&nbsp;&nbsp;<span style="color:#fff;">所持ポイント：</span><span class="point ml_10">140pt</span></div>

         <div align="right" class="topsRight">
         <span class="mr_20"><a href="home.html">HOME</a></span>
         <span class="mr_20"><a href="profile.html">プロフィール編集</a></span>
         <span class="mr_20"><a href="index.html">ログアウト</a></span>
         <span class="mr_20 Userexit"><a href="userexit.html">退会</a></span>
         </div>

	</p>
</div><!--#tops-->
<br>

<div id="profile">

<br>
<center>
    <table width="820" class="mt_10">
    		<tr>
    			<td width="120" valign="top"><img src="usericon/neko.png" width="120"></td>
    			<td valign="top">
    				<table width="600">
    					<tr>
                         <td align="left" width="60">お名前：</td>
                         <td align="left">永田登志雄</td>
                        </tr>
    					<tr>
                         <td>出身：</td>
                         <td>すすきの</td>
                        </tr>
                        <tr>
                        <td colspan="2">
                        <div class="comeback">
                         <div class="comebackIn">こんにちは、私はとても人見知りで普段はおとなしい性格です。</div>
                        </div>
                          </td>
                        </tr>
    				</table>
    			</td>
    		</tr>
    	</table>
</center>

<center>
    <table width="820" class="">
        	<tr>
             <td>
				<br>
       		   <span class="ml_10">コレクション一覧：</span>
                <br>
                <!--コレクション一覧Collection-->
                <div class="Collection ml_30 mt_10">
                	<span class="fl_l mr_5 mt_5"><a href="wallpaper/nature/1.jpg" rel="lightbox1" class="vertical"><img src="wallpaper/nature/1.jpg" width="100" height="60"></a></span>
                    <span class="fl_l mr_5 mt_5"><a href="wallpaper/nature/2.jpg" rel="lightbox1" class="vertical"><img src="wallpaper/nature/2.jpg" width="100" height="60"></a></span>
                    <span class="fl_l mr_5 mt_5"><a href="wallpaper/nature/3.jpg" rel="lightbox1" class="vertical"><img src="wallpaper/nature/3.jpg" width="100" height="60"></a></span>
                    <span class="fl_l mr_5 mt_5"><a href="wallpaper/nature/4.jpg" rel="lightbox1" class="vertical"><img src="wallpaper/nature/4.jpg" width="100" height="60"></a></span>
                    <span class="fl_l mr_5 mt_5"><a href="wallpaper/nature/5.jpg" rel="lightbox1" class="vertical"><img src="wallpaper/nature/5.jpg" width="100" height="60"></a></span>
                    <span class="fl_l mr_5 mt_5"><a href="wallpaper/nature/6.jpg" rel="lightbox1" class="vertical"><img src="wallpaper/nature/6.jpg" width="100" height="60"></a></span>
                    <span class="fl_l mr_5 mt_5"><a href="wallpaper/nature/7.jpg" rel="lightbox1" class="vertical"><img src="wallpaper/nature/7.jpg" width="100" height="60"></a></span>
                    <span class="fl_l mr_5 mt_5"><a href="wallpaper/nature/8.jpg" rel="lightbox1" class="vertical"><img src="wallpaper/nature/8.jpg" width="100" height="60"></a></span>
                    <span class="fl_l mr_5 mt_5"><a href="wallpaper/nature/9.jpg" rel="lightbox1" class="vertical"><img src="wallpaper/nature/9.jpg" width="100" height="60"></a></span>
                    <span class="fl_l mr_5 mt_5"><a href="wallpaper/animal/ani02.jpg" rel="lightbox1" class="vertical"><img src="wallpaper/animal/ani02.jpg" width="100" height="60"></a></span>
                    <span class="fl_l mr_5 mt_5"><a href="wallpaper/animal/ani03.jpg" rel="lightbox1" class="vertical"><img src="wallpaper/animal/ani03.jpg" width="100" height="60"></a></span>
                    <span class="fl_l mr_5 mt_5"><a href="wallpaper/animal/ani04.jpg" rel="lightbox1" class="vertical"><img src="wallpaper/animal/ani04.jpg" width="100" height="60"></a></span>
                    <span class="fl_l mr_5 mt_5"><a href="wallpaper/animal/ani05.jpg" rel="lightbox1" class="vertical"><img src="wallpaper/animal/ani05.jpg" width="100" height="60"></a></span>
                    <span class="fl_l mr_5 mt_5"><a href="wallpaper/animal/ani06.jpg" rel="lightbox1" class="vertical"><img src="wallpaper/animal/ani06.jpg" width="100" height="60"></a></span>
                    <span class="cl_b"></span>
                </div>
                <div class="mt_20 fl_l text_r" style="width:780px;"><a href="collection.html">コレクション一覧を見る</a></div>
                <br><br>
                <!--コレクション一覧Collection-->
             </td>
            </tr>
        </table>
</center>


<br>


<center>
		<div style="width:820px;">

       <div class="boyakiCount m_0 p_0">

        	<form>

            <div class="text_r">
        	表示件数：
        	<select>
            	<option value="Count10">10件</option>
                <option value="Count20">20件</option>
                <option value="Count30">30件</option>
                <option value="Count40">40件</option>
                <option value="Count50">50件</option>
                <option value="CountAll">全件</option>
        	</select>
            <input type="button" value="変更">
            </div><!--text_r-->
        	</form>

        </div><!--boyakiCount-->
        <br>
        <br>

       <table width="820">
    		<tr>

    			<td width="613" align="left">
                	<span class="ml_10 commentDate">6月19日14：42</span>
                	<div class="commentBox">
                    パルムうめぇ～★☆今日はスーパーに言ったらマンゴー味が売ってた!!
                    </div>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="820">
    		<tr>

    			<td width="613" align="left">
                	<span class="ml_10 commentDate">6月19日13：22</span>
                	<div class="commentBox">おなかすいた～お昼なに食べよかな</div>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="820">
    		<tr>

    			<td width="613" align="left">
                	<span class="ml_10 commentDate">6月18日17：42</span>
                	<div class="commentBox">うわっ雨ふってる！だれか傘くれ～</div>
    			</td>
    		</tr>
    	</table>


    <br>

    	<table width="820">
    		<tr>

    			<td width="613" align="left">
                	<span class="ml_10 commentDate">6月18日10：01</span>
                	<div class="commentBox">目の前で痴漢がつかまった！おれはやってませんよ</div>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="820">
    		<tr>

    			<td width="613" align="left">
                	<span class="ml_10 commentDate">6月18日09：10</span>
                	<div class="commentBox">電車遅延。。早起きした意味が。。</div>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="820">
    		<tr>

    			<td width="613" align="left">
                	<span class="ml_10 commentDate">6月17日12：13</span>
                	<div class="commentBox">犬にやたら吠えられてる！！敵だと思われてるのかな。。</div>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="820">
    		<tr>

    			<td width="613" align="left">
                	<span class="ml_10 commentDate">6月16日10：50</span>
                	<div class="commentBox">
                    うぉ～ツカイツリー近くで見るとでけぇ～
                    </div>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="820">
    		<tr>

    			<td width="613" align="left">
                	<span class="ml_10 commentDate">6月15日14：40</span>
                	<div class="commentBox">海鮮ドン！！ドン！！ドン！！</div>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="820">
    		<tr>

    			<td width="613" align="left">
                	<span class="ml_10 commentDate">6月14日21：00</span>
                	<div class="commentBox">体調最悪。。でも肉食いたい！</div>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="820">
    		<tr>

    			<td width="613" align="left">
                	<span class="ml_10 commentDate">6月14日11：00</span>
                	<div class="commentBox">うぉ～～テンション下がる↓↓↓↓</div>
    			</td>
    		</tr>
    	</table>

    	<br>
        <br>


       </div>
       </center>
       <br>


</div><!-- profile -->

<div id="goal"></div><!-- goal -->

</body>
</html:html>