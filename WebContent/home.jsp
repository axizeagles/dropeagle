<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>HOME</title>
	<link rel="stylesheet" href="css/profile.css" type="text/css">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript">
	$(function($) {
		var tab = $('#side'), offset = tab.offset();
		$(window).scroll(function () {
			if($(window).scrollTop() > offset.top) {
				tab.addClass('fixed');
			} else {
				tab.removeClass('fixed');
			}
		});
	});
	function send()
	{
		document.myFORM.submit();
	}
	</script>
<STYLE type="text/css">
<!--

-->
</STYLE>
</head>
<body>

<div id="tops">
	<p>
	 <div class="topsLeft ml_20 fl_l"><!-- 永田登志雄 -->永田登志雄さん Dropへようこそ&nbsp;&nbsp;
	 <span style="color:#fff;">所持ポイント：</span><span class="point"><!-- 140 -->140pt</span></div><!--.topsLeft-->

         <div align="right" class="topsRight">
	         <span class="mr_20"><a href="home.jsp">HOME</a></span>
	         <span class="mr_20"><a href="profile.jsp">プロフィール編集</a></span>
	         <span class="mr_20"><a href="index.jsp">ログアウト</a></span>
	         <span class="mr_20 Userexit"><a href="userexit.jsp">退会</a></span>
         </div><!--.topsRight-->
	</p>
</div><!--#tops-->

<div id="wapper">

<div id="side">
		<center><img src="parts/logo.png"></center>

	    	<div class="login">
	    	<html:form action="/MurmurForm" method="POST" enctype="multipart/form-data">
			   <table cellpadding="0" cellspacing="0" border="0">
			        	<tr>
			             <td>
			             <center><p>いまなにしてる？</p></center>
			             <html:textarea property="mes" cols="30" rows="3" class="boyakiBox"/>ぼやくボタンを押すとぼやきます</textarea>
			             </td>
			            </tr>
			        </table>
                    <center><html:file property="fileUp" accept="image/jpeg, image/gif, image/png"/></center>
		         <br>
		        <center><a href="javaScript:send()"><img src="parts/boyakiButton.png"></a></center>
		        </html:form>
		        <br>
	        </div><!-- login -->

            <br>
            <center>
			<form>
            <select class="mb_10">
                <option value="Count10">自然</option>
                <option value="Count20">動物</option>
                <option value="Count20">世界遺産</option>
            </select>
            </form>
            </center>
			<center>
			<b style="color:red; font-weight:bold; font-size:16px;background-color:#fff;border:2px red solid;padding:2px;">ガチャ：1回300pt</b>
			<a href="gatya.jsp"><img src="parts/button01.png" width="258"></a></center>
            <br>
        	<center>
        	<b style="color:#FF9900; font-weight:bold; font-size:16px;background-color:#fff;border:2px #FF9900 solid;padding:2px;">レアガチャ：1回900pt</b>
        	<a href="raregatya.jsp"><img src="parts/button02.png" width="258"></a></center>

	</div><!-- side -->
	<br><br>

	<div id="container">

        <div class="boyakiCount m_0 p_0">

        <center>
        	<div style="width:660px;text-align:right; font-size:14px;">
        		<!--
        			最新の投稿 | 2 | 3 | 4 |過去の投稿
        		 -->
        		<a href="homeTimelineView.html">過去の投稿</a>
        	</div>
        </center>

        	<br>

        	<form>
            <div class="text_l fl_l">
        	<input type="text" size="40">
            <select>
            	<option value="Count10">キーワードで</option>
                <option value="Count20">日付で</option>
            </select>
            <a href="seachresult.html"><input type="button" value="検索"></a>
            </div>
            <div class="text_r">
        	表示件数：
        	<select>
            	<option value="Count10">10件</option>
                <option value="Count20">20件</option>
                <option value="Count30">30件</option>
                <option value="Count40">40件</option>
                <option value="Count50">50件</option>
        	</select>
        	<a href="homeTimelineView.html">
            <input type="button" value="変更">
            </a>
            </div>
        	</form>
        </div>


        <table width="670" class="mt_10">
    		<tr>
    			<td width="60" valign="top"><a href="profileinfo.html"><img src="usericon/neko.png" width="60" height="60"></a></td>
    			<td>
    				<table width="600">
    					<tr><td align="left">
    						<span style="color:blue; font-weight:bold;"><a href="profileinfo.html"><!-- 永田登志雄 -->永田登志雄</a></span>
    						<span class="ml_10 commentDate">6月19日16：55</span>
    					</td>
    					</tr>
    					<tr>
    					 <td>
    						<div class="commentBox">
    						パルムうめぇ～★☆今日はスーパーに言ったらマンゴー味が売ってた!!
    						</div>
    					 </td>
                        </tr>
    				</table>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="670">
    		<tr>
    			<td width="60" valign="top"><a href="profileinfo2.html"><img src="usericon/neko2.png" width="60" height="60"></a></td>
    			<td>
    				<table width="600">
    					<tr><td align="left"><span style="color:blue; font-weight:bold;"><a href="profileinfo2.html">林偉龍</a></span><span class="ml_10 commentDate">6月19日15：26</span></td></tr>
    					<tr>
    					 <td>
    						<div class="commentBox">
    						今日も元気だ、絶好調の林です！！
    						</div>
    					 </td>
                        </tr>
    				</table>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="670">
    		<tr>
    			<td width="60" valign="top"><a href="profile.html"><img src="usericon/neko3.png" width="60" height="60"s></a></td>
    			<td>
    				<table width="600">
    					<tr><td align="left"><span style="color:blue; font-weight:bold;"><a href="profile.html">風見一慶</a></span><span class="ml_10 commentDate">6月19日14：42</span></td></tr>
    					<tr>
    					 <td>
    						<div class="commentBox">
    						今日は新しいいつも行く定食屋で、レモンラーメンを発見！！上にのった鶏肉もなかなかのボリュームで、暑い日にぴったり
    						</div>
    					 </td>
                        </tr>
    				</table>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="670">
    		<tr>
    			<td width="60" valign="top"><a href="profile.html"><img src="usericon/radiokaikan.jpg" width="60" height="60"></a></td>
    			<td>
    				<table width="600">
    					<tr><td align="left"><span style="color:blue; font-weight:bold;"><a href="profile.html">森谷和俊</a></span><span class="ml_10 commentDate">6月18日22：30</span></td></tr>
    					<tr>
    					 <td>
    						<div class="commentBox">
    						ラブライブ☆
    						</div>
                            <div class="commentIMG">
                            <img src="dropgraphic/lovelive.jpg">
                            </div>
    					 </td>
                        </tr>
    				</table>
    			</td>
    		</tr>
    	</table>


    <br>

    	<table width="670">
    		<tr>
    			<td width="60" valign="top"><a href="profile.html"><img src="usericon/neko4.jpg" width="60" height="60"></a></td>
    			<td>
    				<table width="600">
    					<tr><td align="left"><span style="color:blue; font-weight:bold;"><a href="profile.html">香月兼一</a></span><span class="ml_10 commentDate">6月18日20：15</span></td></tr>
    					<tr>
    					 <td>
    						<div class="commentBox">
    						ひじを打ってしまった。。。
    						</div>
    					 </td>
                        </tr>
    				</table>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="670">
    		<tr>
    			<td width="60" valign="top"><a href="profile.html"><img src="usericon/im@s01.jpg" width="60" height="60"></a></td>
    			<td>
    				<table width="600">
    					<tr><td align="left"><span style="color:blue; font-weight:bold;"><a href="profile.html">西山広敏</a></span><span class="ml_10 commentDate">6月18日19：42</span></td></tr>
    					<tr>
    					 <td>
    						<div class="commentBox">
    						脳が揺れる～
    						</div>
    					 </td>
    				</table>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="670">
    		<tr>
    			<td width="60" valign="top"><a href="profile.html"><img src="usericon/jojo.jpg" width="60" height="60"></a></td>
    			<td>
    				<table width="600">
    					<tr><td align="left"><span style="color:blue; font-weight:bold;"><a href="profile.html">田頭克矩</a></span><span class="ml_10 commentDate">6月17日12：34</span></td></tr>
    					<tr>
    					 <td>
    						<div class="commentBox">
    						あぁ～あそびてぇ～海～
    						</div>
                            <div class="commentIMG">
                            <img src="dropgraphic/userphoto01.jpg">
                            </div>
    					 </td>
                        </tr>
    				</table>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="670">
    		<tr>
    			<td width="60" valign="top"><a href="profile.html"><img src="usericon/odenkun.jpg" width="60" height="60"></a></td>
    			<td>
    				<table width="600">
    					<tr><td align="left"><span style="color:blue; font-weight:bold;"><a href="profile.html">深谷俊</a></span><span class="ml_10 commentDate">6月17日06：12</span></td></tr>
    					<tr>
    					 <td>
    						<div class="commentBox">
    						おでん君いつもありがとう。感謝しています。
    						</div>
    					 </td></tr>
    				</table>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="670">
    		<tr>
    			<td width="60" valign="top"><a href="profile.html"><img src="usericon/marshall.jpg" width="60" height="60"></a></td>
    			<td>
    				<table width="600">
    					<tr><td align="left"><span style="color:blue; font-weight:bold;"><a href="profile.html">松本諒</a></span><span class="ml_10 commentDate">6月17日04：55</span></td></tr>
    					<tr>
    					 <td>
    						<div class="commentBox">
    						やっべー！！まえのりだね！！
    						</div>
    					 </td>
                        </tr>
    				</table>
    			</td>
    		</tr>
    	</table>

    <br>

    	<table width="670">
    		<tr>
    			<td width="60" valign="top"><a href="profile.html"><img src="usericon/gyuutan.jpg" width="60" height="60"></a></td>
    			<td>
    				<table width="600">
    					<tr><td align="left"><span style="color:blue; font-weight:bold;"><a href="profile.html">横田海</a></span><span class="ml_10 commentDate">6月17日04：42</span></td></tr>
    					<tr>
    					 <td>
    						<div class="commentBox">
    						この水すっげーまずい！！
    						</div>
    					 </td>
                        </tr>
    				</table>
    			</td>
    		</tr>
    	</table>

    <br>

    <center><div style="width:660px;text-align:right; font-size:14px;"><a href="homeTimelineView.jsp">過去の投稿</a></div></center><br>

</div><!-- /#container -->

<div id="goal"></div><!-- goal -->

</div><!-- wapper -->

</body>
</html:html>