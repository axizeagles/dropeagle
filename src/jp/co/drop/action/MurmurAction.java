package jp.co.drop.action;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.drop.dao.DbUtil;
import jp.co.drop.form.MurmurForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;


public final class MurmurAction extends Action {
 public ActionForward execute(ActionMapping mapping,
        ActionForm form,
        HttpServletRequest req,
        HttpServletResponse res) throws Exception{

	  //(2)アクション・フォームBeanオブジェクトの取得
	  MurmurForm efuf = (MurmurForm)form;

	  //(3)アクセスメソッドを使用してFormFileオブジェクトの取得
	  FormFile fileUp = efuf.getFileUp();

	  //(4)getInputStreamメソッドを使用し、入力ストリームを取得
	  InputStream is = fileUp.getInputStream();

	  //(5)入力ストリームをバッファリング
	  BufferedInputStream inBuffer = new BufferedInputStream(is);

	  //(6)ファイルのアップロード先を指定して、出力ストリームを生成
	  FileOutputStream fos = new FileOutputStream
	                         ("C:/workspace/drop/WebContent/dropgraphic/"
	                         + fileUp.getFileName());

	  //(7)出力ストリームをバッファリング
	  BufferedOutputStream outBuffer = new BufferedOutputStream(fos);

	  int contents = 0;

	  //(8)入力データがなくなるまで入出力処理を実行
	  while ((contents = inBuffer.read()) != -1) {
		  outBuffer.write(contents);
	  }

	  outBuffer.flush();
	  inBuffer.close();
	  outBuffer.close();

	  //(9)一時領域のアップロードデータを削除
	  fileUp.destroy();

	  //データベース接続
	  Class.forName("org.postgresql.Driver");
	  Connection con =DbUtil.connect();

	  String tittle = efuf.getMes();
	  String file = fileUp.getFileName();

	  //データベース接続
	  String sql0 = "SELECT * FROM murmur;";
	  PreparedStatement stmt0 = con.prepareStatement(sql0);
	  ResultSet rs0 = stmt0.executeQuery();

	  //logintablephotoを全検索
	  String sqlSelect2 = "INSERT INTO murmur (id,photono) VALUES(?,?)";
	  PreparedStatement stmt2 = con.prepareStatement(sqlSelect2);
	  stmt2 = con.prepareStatement(sqlSelect2);

	  stmt2.setString(1, Mes);
	  stmt2.setString(2, file);
	  stmt2.executeUpdate();

	  //データベース切断
	  con.close();


	  return (mapping.findForward("success"));
 	}
}
