package jp.co.drop.form;

public class WithdrawConfirmForm {
	private String[] answerlist;
	private String answer;
	private String userid;
	private String pass;

	public String[] getAnswerlist() {
		return answerlist;
	}
	public void setAnswerlist(String[] answerlist) {
		this.answerlist = answerlist;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}

}
