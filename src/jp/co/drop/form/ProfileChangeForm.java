package jp.co.drop.form;

public class ProfileChangeForm {
	private String userid;
	private String username;
	private String pass;
	private String passconfirm;
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getPassconfirm() {
		return passconfirm;
	}
	public void setPassconfirm(String passconfirm) {
		this.passconfirm = passconfirm;
	}

}
