package jp.co.drop.form;

public class UserSearchForm {
	private boolean searchflg;
	private String keyword;

	public boolean isSearchflg() {
		return searchflg;
	}
	public void setSearchflg(boolean searchflg) {
		this.searchflg = searchflg;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
}
