package jp.co.drop.form;

import jp.co.drop.utility.UserInfo;

public class UserMurmurViewForm {
	private UserInfo userinfo;
	//private ArrayList<MurmurEntitiy> murmurlist;
	private String[] checkmurmur;
	public UserInfo getUserinfo() {
		return userinfo;
	}
	public void setUserinfo(UserInfo userinfo) {
		this.userinfo = userinfo;
	}
	public String[] getCheckmurmur() {
		return checkmurmur;
	}
	public void setCheckmurmur(String[] checkmurmur) {
		this.checkmurmur = checkmurmur;
	}

	/*
	public ArrayList<MurmurEntity> getMurmurlist(){
		return murmurlist;
	}
	public void setMurmurlist(ArrayList<MurmurEntity> murmurlist){
		this.murmurlist = murmurlist;
	}
	*/

}
