package jp.co.drop.form;
import java.util.ArrayList;

import jp.co.drop.utility.UserInfo;

public class IndexForm {
	private String userid;
	private String pass;
	private ArrayList<UserInfo> userlist;
	private String[] checkuser;

	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public ArrayList<UserInfo> getUserlist() {
		return userlist;
	}
	public void setUserlist(ArrayList<UserInfo> userlist) {
		this.userlist = userlist;
	}
	public String[] getCheckuser() {
		return checkuser;
	}
	public void setCheckuser(String[] checkuser) {
		this.checkuser = checkuser;
	}



}
