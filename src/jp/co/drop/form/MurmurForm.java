package jp.co.drop.form;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class MurmurForm extends ActionForm {
	private String mes;
	private String picture;
	private FormFile fileUp;

	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public FormFile getFileUp() {
		return fileUp;
	}
	public void setFileUp(FormFile fileUp) {
		  this.fileUp = fileUp;
	}

}
