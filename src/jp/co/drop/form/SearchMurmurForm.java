package jp.co.drop.form;

public class SearchMurmurForm {
	private String keyword;
	private boolean searchflg;
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public boolean isSearchflg() {
		return searchflg;
	}
	public void setSearchflg(boolean searchflg) {
		this.searchflg = searchflg;
	}

}
