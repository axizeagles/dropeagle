package jp.co.drop.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DbUtil {

/*
 * DB接続および切断メソッド
 */
	public static Connection connect() throws SQLException,ClassNotFoundException{
		//JDBCドライバをロード
		Class.forName("org.postgresql.Driver");
		//データベースへの接続
	    return DriverManager.getConnection("jdbc:postgresql:axizdb","axizuser","axiz");
	}
	public static void disconect(Connection inputCon) throws SQLException{
		//DB切断
		inputCon.close();
	}

	/*
	 * ステートメント作成および切断メソッド
	 */
	public static Statement stateConnect(Connection inputCon) throws SQLException,ClassNotFoundException{
		return inputCon.createStatement();
	}
	public static void stateDisConnect(Statement inputStmt) throws SQLException{
		inputStmt.close();
	}

}
